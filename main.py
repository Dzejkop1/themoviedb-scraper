from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import csv
import json

def simple_get(url):
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        print('Error during requests to {0} : {1}'.format(url, str(e)))
        return None

def is_good_response(resp):
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)

def parse_movie(raw_html, link):
    html = BeautifulSoup(raw_html, 'html.parser')
    title = html.select('.title a h2')[0].text
    description = html.select('.overview p')[0].text
    release_year = html.select('.title .release_date')[0].text
    facts = parse_movie_facts(html.select('.facts')[0])
    cast = parse_cast(html.select('ol.people li'))
    social_links = [l['href'] for l in html.select('a.social_link')]
    fb_link = [l for l in social_links if 'facebook' in l][0]

    return {'title': title, 'description': description, 'release_year': release_year, 'facts': facts, 'cast': cast, 'link': link, 'fb_link': fb_link}

def parse_movie_facts(facts):
    facts = facts.select('p')
    parsed_facts = {}
    for f in facts:
        if f.select('strong bdi'):
            category = f.select('strong bdi')[0]
            parsed_facts[category.text] = str(category.parent.next_sibling)

    clean_facts(parsed_facts)

    return parsed_facts

def clean_facts(facts):
    for f in facts:
        facts[f] = facts[f].strip()

def parse_cast(cast):
    people = [c.select('p a')[0] for c in cast]

    actors = [p.text for p in people if p.parent.parent.get('class')[0] == 'card']
    crew = [p for p in people if p.parent.parent.get('class')[0] == 'profile']
    crew = [p.parent.parent for p in crew]
    crew = [{'role': p.select('.character')[0].text, 'person': p.select('a')[0].text} for p in crew]
    actors = [{'role': 'Actor', 'person': p} for p in actors]

    return actors + crew

def get_discovery_link(page=1, year=2018):
    return 'https://www.themoviedb.org/discover/movie?page={}&primary_release_year={}&sort_by=popularity.desc'.format(page, year)

def get_all_movie_links(years=[2018, 2017, 2016], pages=[1, 2, 3, 4, 5]):
    movie_links = []
    for year in years:
        for page in pages:
            url = get_discovery_link(page, year)
            raw = simple_get(url)
            soup = BeautifulSoup(raw, 'html.parser')
            links = [m.get('href') for m in soup.select('a.title.result')]

            movie_links += links

    return ['https://www.themoviedb.org' + l for l  in movie_links]

def main():
    import argparse
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    links = subparsers.add_parser('links', help='Scrap movie links')
    movies = subparsers.add_parser('movies', help='Scrap movie data')
    movie = subparsers.add_parser('movie', help='Scrap a single movie')

    parser.set_defaults(func=None)

    links.add_argument('out', type=str, metavar='O', help='Target text file.')
    links.add_argument('--page-range', '-p', metavar='P', type=int, nargs=2, help='Page range (inclusive) default is 1-5', default=[1, 5])
    links.add_argument('--year-range', '-y', metavar='Y',
        type=int, nargs=2,
        help='Year range (inclusive) default is 2016-2018. NOTE: First value must be smaller than the second.',
        default=[2016, 2018])
    links.set_defaults(func=get_links)

    movies.add_argument('--json-out', '-j', type=str, metavar='JSON', help='Target json file.')
    movies.add_argument('--csv-out', '-c', type=str, metavar='CSV', help='Target csv file.')
    movies.add_argument('links', type=str, metavar='LINKS', help='File to get links from')
    movies.set_defaults(func=get_movies)

    movie.add_argument('url', type=str, help='Url to parse')
    movie.set_defaults(func=get_movie)

    args = parser.parse_args()
    if args.func:
        args.func(args)

def get_movie(args):
    raw = simple_get(args.url)
    info = parse_movie(raw, args.url)

    print (info)

def get_movies(args):
    get_movies_internal(args.links, args.csv_out, args.json_out)

def get_movies_internal(links, csv_out, json_out):
    movies = []
    with open(links, 'r') as links_file:
        for l in links_file:
            raw = simple_get(l.strip())
            try:
                movie_info = parse_movie(raw, l.strip())
                movies.append(movie_info)
            except:
                print("No movie at {}".format(l.strip()))
                pass

    if csv_out:
        with open(csv_out, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=',',
                                    quotechar='"', quoting=csv.QUOTE_MINIMAL)

            writer.writerow(['title', 'description', 'release_year', 'facts', 'cast', 'link', 'fb_link'])
            for movie in movies:
                writer.writerow([
                    movie['title'],
                    movie['description'],
                    movie['release_year'],
                    json.dumps(movie['facts']),
                    json.dumps(movie['cast']),
                    movie['link'],
                    movie['fb_link']
                ])

    if json_out:
        with open(json_out, 'w') as jsonfile:
            jsonfile.write(json.dumps(movies))

def get_links(args):
    years = list(range(args.year_range[0], args.year_range[1] + 1))
    pages = list(range(args.page_range[0], args.page_range[1] + 1))
    get_links_internal(args.out, years, pages)

def get_links_internal(out, years=[2016, 2017, 2018], page_range=[1, 6]):
    with open(out, 'w') as target:
        links = get_all_movie_links(years, page_range)
        for l in links:
            target.write(l + "\n")

if __name__ == '__main__':
    main()
