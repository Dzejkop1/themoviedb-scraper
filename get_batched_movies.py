import argparse
import os
from pathlib import Path
from main import get_movies

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('links_dir', type=str, help='Directory with link files')
    parser.add_argument('target_dir', type=str, help='Target directory')
    parser.add_argument('--mode', choices=['csv', 'json'], help='Saving mode default is json', default='json')

    args = parser.parse_args()

    for fname in os.listdir(args.links_dir):
        target_file = Path(args.target_dir) / Path(fname).with_suffix('.' + args.mode)
        source_file = Path(args.links_dir) / Path(fname)
        if args.mode == 'csv':
            get_movies(source_file, target_file, None)
        if args.mode == 'json':
            get_movies(source_file, None, target_file)

if __name__ == '__main__':
    main()