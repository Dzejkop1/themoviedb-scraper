import argparse
import json
import re
from urllib.parse import urlparse
from bs4 import BeautifulSoup

from main import simple_get

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('source', type=str)
    parser.add_argument('target', type=str)
    parser.add_argument('--verbose', '-V', action='store_true')

    args = parser.parse_args()

    new_movies = []
    with open(args.source, 'r') as f:
        data = json.load(f)

        for i, movie in enumerate(data):
            if args.verbose:
                print ('Parsing movie {}/{} - {}'.format(i, len(data), movie["title"]))
            raw = simple_get(movie['link'])
            html = BeautifulSoup(raw, 'html.parser')

            image_srcset = html.select('img.poster')[0]['srcset'].split(' ')

            if len(image_srcset) > 2:
                movie["image_url"] = image_srcset[2]
            else:
                movie['image_url'] = image_srcset[0]

        with open(args.target, 'w') as t:
            json.dump(data, t)


if __name__ == '__main__':
    main()